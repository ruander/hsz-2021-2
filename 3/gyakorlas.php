<?php
/*
 * 13.Állítson elő php kóddal egy űrlapot, amely tartalmaz egy szövegmezőt, egy elküldő gombot, egy jelölő négyzetet és egy lenyíló listát, amelyben a hét napjait sorolja fel.
 */
$style = '<style>
form {
    max-width:400px;
    margin: 0 auto;
    display:flex;
    flex-direction:column;
}
form > label {
   display:flex;
   flex-direction:column;
   margin:5px 0; 
}
option {
   text-transform:capitalize;
}
</style>';//stílusok
//stílusok kiírása 1 lépésben
echo $style;

$form = '<form method="get" action="feldolgoz.php">';//űrlap nyitás

$days = [//napok tömbje (halmaz)
    'monday' => 'hétfő',
    'tuesday' => 'kedd',
    'wednesday' => 'szerda',
    'thursday' => 'csütörtök',
    'friday' => 'péntek',
    'saturday' => 'szombat',
    'sunday' => 'vasárnap'
];
$day_selector = '<select name="day">
                    <option value="">--válassz--</option>';
//opciók a tömb segítségével (foreach)
foreach($days as $k => $v ){
    //<option value="monday">Hétfő</option>
    $day_selector .= '<option value="'.$k.'">'.$v.'</option>';
}

$day_selector .= '</select>';

//var_dump($days);
$form .= '<label>
            <span>Nap</span>
          '.$day_selector.'  
          </label>';

$form .= '<label>
            <span>üzenet</span>
            <textarea name="message" placeholder="üzenet..."></textarea>
          </label>';//szövegmező

$form .= '<div>
            <label>
            <input type="checkbox" name="terms"> <span>Elolvastam és megértettem az <a href="#" target="_blank">adatkezelési tájékoztató</a>t!</span>
</label>
          </div>';

$form .= '<button>Küldés</button>';//operátor: .= -> hozzáfűzi a bal oldalon található kifejezéshez a jobb oldali kifejezést (string)
$form .= '</form>';//űrlap
//kiírás 1 lépésben:
echo $form;

//12.Írjon egy php programot, amely előállít egy XxY cellát tartalmazó táblázatot és mindegyik cellában a Google szót helyezi el a cellák jobb felső sarkába.
$sor = 8;//sorok száma
$oszlop = 8;//oszlopok száma

$table = '<table border="1">';
//külső ciklus a soroknak
for($i=1;$i<=$sor;$i++){
    $table .= '<tr>';//sor nyitása
    for($j=1;$j<=$oszlop;$j++){
        $table .= '<td>google</td>';//egy cella
    }
    $table .= '</tr>';//sor zárása
}
$table .= '</table>';

//kiírás egy lépésben
echo $table;

/*
19.Írjon egy programot ciklus utasítást használva, amely az alábbi elrendezésű szöveget írja ki a képernyőre:
2
22
222
2222
*/
//sorok
for($i=1;$i<=4;$i++){
    //belső ciklus a 2eseknek (sornyi)
    for($j=1;$j<=$i;$j++){
        echo '2';
    }

    //2esek után sortörés
    echo '<br>';
}
for($i=1;$i<=4;$i++){
    //str_repeattel
    echo str_repeat('0',$i).'<br>';
}
