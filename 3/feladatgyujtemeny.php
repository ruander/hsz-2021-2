<?php
//1. Készítsen egy olyan ciklust, amely egymás után menü feliratokat rak ki.
$menupontok = [
    'home','about','services','contact'
];

$menu = '<nav>
            <ul class="mainmenu">';//nav és ul nyitása
//menüpontok
foreach ($menupontok as $menuId => $menuItem){
    $menu .='<li><a href="?menu='.$menuId.'">'.$menuItem.'</a></li>';//elemek a tömbből
}

$menu .='</ul></nav>';//ul és nav zárása

echo $menu;

//2. Készítsünk programot, amely kiszámolja az első 100 darab. pozitív egész szám összegét, majd kiírja az eredményt. (Az összeg kiszámolásához vezessünk be egy változót, amelyet a program elején kinullázunk, a ciklusmagban pedig mindig hozzáadjuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, 4, ..., 100 számokat.)
$sum = 0;//ebbe gyűjtjük az összeget
for($i=1;$i<=100;$i++){
    $sum += $i;//operátor $sum += $i -> $sum = $sum +$i //-=,*=,/=
}
echo "<div>A számok összege 1-100ig -> $sum</div>";

//3. Készítsünk programot, amely kiszámolja az első 7 darab. természetes szám szorzatát egy ciklus segítségével. (A szorzat kiszámolásához vezessünk be egy változót, amelyet a program elején beállítunk 1-re, a ciklusmagban pedig mindig hozzászorozzuk a ciklusváltozó értékét, tehát sorban az 1, 2, 3, ..., 7 számokat.)
$mtpl = 1;//ide gyüjtjük a szorzatot
for($i=1;$i<=7;$i++){
    $mtpl *= $i;
}

echo "<div>A számok szorzata 1-7 (7!) -> $mtpl</div>";

//4. Készítsünk programot, amely kiszámolja az első 100 darab. páros szám összegét (A ciklus vegyük egytől ötvenig, majd a ciklusmagban vegyük a ciklusváltozó kétszeresét - így megkapjuk a páros számokat. Ezeket hasonlóan adjuk össze, mint az első feladatban).

$sum = 0;//kinullázuk hogy ne az előző értékről menjen tovább
for($i=1;$i<=50;$i++){
    $sum += $i*2;
}
echo "<div>A páros számok összege 1-100ig -> $sum</div>";
//pazarló megoldás de ugyanazt eredményezi
$sum = 0;//kinullázuk hogy ne az előző értékről menjen tovább
for($i=1;$i<=100;$i++){
    //ha páros az i, akkor hozzáadjuk
    if($i%2===0){
        $sum += $i;
    }

}
echo "<div>A páros számok összege 1-100ig -> $sum</div>";
//2 vel növeléssel
$sum = 0;//kinullázuk hogy ne az előző értékről menjen tovább
for($i=2;$i<=100;$i+=2){
    $sum += $i;
}
echo "<div>A páros számok összege 1-100ig -> $sum</div>";
//5. @todo: HF: 5ös, ahány féle képpen csak tudod, + txt feladatok összes
