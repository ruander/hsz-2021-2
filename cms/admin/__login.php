<?php
//erőforrások
require "../config/database.php";/** @var $link */
require "../config/functions.php";
require "../config/settings.php";//beállítások betöltése
//munkafolyamat elemek (SESSION kezelés alapok)
/**
 * $_SESSION szuperglobális tömb - csak _start után
 * session_ eljárások
 */
session_start();//mf indítása

//mf azonosító
$info = '<div class="info">Töltse ki a belépési adatokat!</div>';

if (!empty($_POST)) {
    //ha stimmel -> index
    if (login()) {
        //eltároljuk a belépéskor gyártott azonosítást
        header('location:index.php');
        exit();
    } else {
        //ha nem -> hibaüzenet
        $info = '<div class="info">Nem érvényes email/jelszó páros!</div>';
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ruander PHP tanfolyam - CMS - beléptetés</title>
</head>
<body>
<form method="post">
    <?php echo $info; ?>
    <label>
        <span>Email</span>
        <input type="text" name="email" placeholder="email@cim.hu" value="<?php echo getValue('email'); ?>">
    </label>

    <label>
        <span>Jelszó</span>
        <input type="password" name="password" placeholder="******" value="">
    </label>
    <button>Belépek</button>
</form>
</body>
</html>
