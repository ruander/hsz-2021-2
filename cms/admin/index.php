<?php
//erőforrások
require "../config/database.php";/** @var $link */
require "../config/functions.php";
require "../config/settings.php";//beállítások betöltése
session_start();//mf indítása
//var_dump($_SESSION, session_id());
//melyik oldal (menüpont)aktív?
$p = filter_input(INPUT_GET, 'p', FILTER_VALIDATE_INT) ?: 0;//ha nincs ilyen, akkor legyen a 0 (vezérlőpult)
$baseURL = 'index.php?p='.$p;//alap url (benne van a modul id is)
//'kiléptetés' ha kell
if (filter_input(INPUT_GET, 'logout')) {
    logout();
}

//védett felület (beléptetés)
$auth = auth();//érvényes belépés ellenőrzése

if (!$auth) {
    header('location:login.php');
    exit();
}

//modul kiválasztása és betöltése
$moduleFile = MODULE_DIR . ADMIN_MENU[$p]['module_name'] . MODULE_EXT;
//ha létezik ilyen modul, töltsük be
if (file_exists($moduleFile)) {
    include $moduleFile; //$output kialakítása
} else {
    $output = 'nincs ilyen modul:' . $moduleFile;
}

$userbar = '<div class="userbar info">Üdvözlet <b>' . $_SESSION['userdata']['name'] . '</b>! | <a href="?logout=true">kilépés</a></div>';

$adminMenu = '<nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">';
//menüpontok
foreach (ADMIN_MENU as $menuId => $menuItem) {
    $adminMenu .= '<li class="nav-item">
                        <a href="?p=' . $menuId . '" class="nav-link">
                            <i class="nav-icon ' . $menuItem['icon_class'] . '"></i>
                            <p>
                                ' . $menuItem['title'] . '
                                <!--<span class="right badge badge-danger">New</span>-->
                            </p>
                        </a>
                    </li>';
}
$adminMenu .= '</ul></nav>';
//$output = "<h1>".ADMIN_MENU[$p]['title']."</h1>";//az aktív menüpont címe
?><!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Adminisztrációs felület - <?php echo ADMIN_MENU[$p]['title']; ?></title>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/all.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="css/icheck-bootstrap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="css/adminlte.min.css">
    <?php
    /*ha van modul plusz stilus, írjuk ide*/
    /*if(isset($additional_styles)){
        echo $additional_styles;
    }*/
    echo $additional_styles ?? '';
    //@todo HF: Cikk egy képpel.... (milyen adatbázis táblát hoznál létre [terv] articles (pl: id int(11) unsigned auto_increment ....)
    ?>
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
           <!-- <li class="nav-item d-none d-sm-inline-block">
                <a href="../../index3.html" class="nav-link">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Contact</a>
            </li>-->
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="../../index3.html" class="brand-link">
            <!--<img src="../../dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
                 style="opacity: .8">-->
            <span class="brand-text font-weight-light">Ruander CMS admin</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <?php echo $userbar; ?>
                <!--<div class="image">
                    <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">Alexander Pierce</a>
                </div>-->
            </div>

            <!-- SidebarSearch Form -->
            <div class="form-inline">
                <div class="input-group" data-widget="sidebar-search">
                    <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                           aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-sidebar">
                            <i class="fas fa-search fa-fw"></i>
                        </button>
                    </div>
                </div>
            </div>

            <!-- Sidebar Menu -->

                    <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->
            <?php echo $adminMenu; ?>

            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Oldal főcím</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Blank Page</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">modulcím</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <?php echo $output;//kimenetek (tartalom kiírása) ?>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    modul lábléc
                </div>
                <!-- /.card-footer-->
            </div>
            <!-- /.card -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="float-right d-none d-sm-block">
            <b>Version</b> 3.1.0
        </div>
        <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="js/adminlte.min.js"></script>
<?php
echo $additional_scripts ?? '';
?>
</body>
</html>
