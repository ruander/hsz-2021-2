<?php
//erőforrások
require "../config/database.php";/** @var $link */
require "../config/functions.php";
require "../config/settings.php";//beállítások betöltése
session_start();//mf indítása
//var_dump($_SESSION, session_id());
//melyik oldal (menüpont)aktív?
$p = filter_input(INPUT_GET, 'p', FILTER_VALIDATE_INT) ?: 0;//ha nincs ilyen, akkor legyen a 0 (vezérlőpult)
$baseURL = 'index.php?p='.$p;//alap url (benne van a modul id is)
//'kiléptetés' ha kell
if (filter_input(INPUT_GET, 'logout')) {
    logout();
}

//védett felület (beléptetés)
$auth = auth();//érvényes belépés ellenőrzése

if (!$auth) {
    header('location:login.php');
    exit();
}

//modul kiválasztása és betöltése
$moduleFile = MODULE_DIR . ADMIN_MENU[$p]['module_name'] . MODULE_EXT;
//ha létezik ilyen modul, töltsük be
if (file_exists($moduleFile)) {
    include $moduleFile; //$output kialakítása
} else {
    $output = 'nincs ilyen modul:' . $moduleFile;
}

$userbar = '<div class="userbar">Üdvözlet <b>' . $_SESSION['userdata']['name'] . '</b>! | <a href="?logout=true">kilépés</a></div>';

$adminMenu = '<nav><ul class="mainmenu">';
//menüpontok
foreach (ADMIN_MENU as $menuId => $menuItem) {
    $adminMenu .= '<li>
                    <a href="?p=' . $menuId . '" 
                        class="' . $menuItem['icon_class'] . '">' . $menuItem['title'] . '</a>
                        </li>';
}
$adminMenu .= '</ul></nav>';
//$output = "<h1>".ADMIN_MENU[$p]['title']."</h1>";//az aktív menüpont címe
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Adminisztrációs felület - <?php echo ADMIN_MENU[$p]['title']; ?></title>
    <?php
    /*ha van modul plusz stilus, írjuk ide*/
    /*if(isset($additional_styles)){
        echo $additional_styles;
    }*/
    echo $additional_styles ?? '' ;
    //@todo HF: Cikk egy képpel.... (milyen adatbázis táblát hoznál létre [terv] articles (pl: id int(11) unsigned auto_increment ....)
    ?>
</head>
<body>
<?php
//kiírások
echo $userbar;
echo $adminMenu;
echo $output;
?>
</body>
</html>

