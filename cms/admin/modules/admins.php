<?php
/**
 * @var $baseURL string //index
 * @var $link resource
 */
if(!isset($link)){//önálló futtatás elleni védelem (nincs db kapcsolat)
    header('location:index.php');
    exit();
}
//törlés action feldolgozása ha kell (url paraméterek feldolgozása)
$action = filter_input(INPUT_GET,'action');
$tid = filter_input(INPUT_GET,'id',FILTER_VALIDATE_INT)?:null;//target id

//hibakezelés/műveletek (logika)
if (!empty($_POST)) {//csak akkor foglalkozunk a postal ha van benne valami
    echo '<pre>POST: ' . var_export($_POST, true) . '</pre>';
    //adatok feldolgozása
    //hibakezelés
    $hiba = [];//ide gyüjtjük a hibákat
    //név minimum 3 karakter
    $name = filter_input(INPUT_POST, 'name');
    $name = strip_tags($name);//védelem TAGek ellen (script)
    $name = trim($name);//távolítsuk a space-ket
    $name = mysqli_real_escape_string($link,$name);//mysql injection elleni védelem a mezőre
    //var_dump(strlen($name));
    if (mb_strlen($name, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //email kötelező azaz minimum látszódjon emailnek
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    }else{
        //létezik e már?
        $qry = "SELECT id FROM admins WHERE email ='$email' LIMIT 1";

        $result = mysqli_query($link,$qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        var_dump($row,$tid);
        //ha van row, foglalt az email
        if($row !== null && $row[0] != $tid){
            $hiba['email'] = '<span class="error">Már regisztrált email!</span>';
        }
    }

    //jelszó
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    //jelszó 1 legalább 6 karakter (mb!) EDIt esetén csak akkor nyulunk a jelszóhoz ha az 1 mező legalább 1 karaktert tartalmaz
    if(strlen($password) > 0 || $action == 'create') {
        if (mb_strlen($password, 'utf-8') < 6) {
            $hiba['password'] = '<span class="error">Legalább 6 karakter kell legyen!</span>';
        } elseif ($password !== $repassword) {//ha nem egyezik a 2 jelszó
            $hiba['repassword'] = '<span class="error">Jelszavak nem egyeztek!</span>';
        } else {
            //jelszó biztonságos kódolása
            $password = password_hash($password, PASSWORD_BCRYPT);
        }
    }

    //státusz
    $status = filter_input(INPUT_POST,'status') ? 1 : 0;//ha ki volt pipálva, 1 egyébként 0


    if (empty($hiba)) {
        //minden ok... adatok rendezése
        $data = [
            'name' => $name,
            'email' => $email,
            'status' => $status
        ];
        //jelszó csak akkor legyen a data tömbben ha valóban kell módosítani
        if($password){
            $data['password'] = $password;
        }

        if($action == 'update'){
            //módosításkor
            $data['time_updated'] = date('Y-m-d H:i:s');
            echo '<pre>data: ' . var_export($data, true) . '</pre>';
            //update set query rész a data tömb bejárásával
            $updateElements =  [];
            foreach($data as $fieldName => $value){
                $updateElements[] = "`$fieldName` = '$value'";
            }
            //echo '<pre>data: ' . var_export($updateElements, true) . '</pre>';
            $qry = "UPDATE admins SET ".implode(',',$updateElements)." WHERE id = $tid LIMIT 1";

        }else{
            $data['time_created'] = date('Y-m-d H:i:s');
            echo '<pre>data: ' . var_export($data, true) . '</pre>';
            //új felvitel az admins táblába
            $qry = "INSERT INTO
  `admins` (`name`, 
            `email`, 
            `password`, 
            `status`, 
            `time_created`)
  VALUES ('{$data['name']}', 
          '{$data['email']}', 
          '{$data['password']}', 
          '{$data['status']}', 
          '{$data['time_created']}');";
        }

        //futtatás
        mysqli_query($link,$qry) or die(mysqli_error($link));
        //átirányítás a listázásra (most admins.php)
        header('location:'.$baseURL);//irány a listázás
        exit();
    }
}


switch($action){
    case 'delete':
        if($tid){
            //echo 'Törlünk:'.$tid;
            //record törlése:
            mysqli_query($link,"DELETE FROM admins WHERE id = $tid LIMIT 1") or die(mysqli_error($link));
        }
        header('location:'.$baseURL);//hogy az url paraméterek 'eltünjenek' vissza a listázásra
        exit();
        break;
    case 'update':

        //sor lekérése
        $qry = "SELECT name,email,status FROM admins WHERE id = '$tid' LIMIT 1";
        $result = mysqli_query($link,$qry) or die(mysqli_error($link));
        $row = mysqli_fetch_assoc($result);
        echo '<pre>'.var_export($row,true).'</pre>';
        $formTitle = "<h2>Módosítás -- [$tid] -- </h2>";


        //break;
    case 'create':
        $formTitle = $formTitle ?? '<h2>Új felvitel</h2>';//űrlap címe
        //ha nincs row, akkor létezzen és legyen egy üres tömb, ha létezik akkor legyen maga
        $row = $row ?? [];//isset($row) ? $row : [];
        //űrlap
        $form = '<form method="post">'.$formTitle;//form elemek változója
//név
        $form .= '<label>
        <span>Név<sup>*</sup></span>
        <input
            type="text"
            name="name"
            placeholder="Gipsz Jakab"
            value="' . getValue('name', $row) . '">';//db adat, post, vagy üres
//ha létezik a hiba elem, akkor hozzáfűzzük a formhoz
        $form .= getError('name');
        $form .= '</label>';

//email ...
        $form .= '<label>
    <span>Email<sup>*</sup></span>
    <input
            type="text"
            name="email"
            placeholder="your@email.com"
            value="' . getValue('email', $row) . '"
    >';

//ha létezik a hiba elem, akkor hozzáfűzzük a formhoz
        $form .= getError('email');

        $form .= '</label>
<!--Jelszó 1-->
<label>
    <span>Jelszó<sup>*</sup></span>
    <input
            type="password"
            name="password"
            placeholder="******"
            value=""
    >';

//ha létezik a hiba elem, akkor hozzáfűzzük a formhoz
        $form .= getError('password');

        $form .= '</label>
<!--Jelszó újra-->
<label>
    <span>Jelszó úrja<sup>*</sup></span>
    <input
            type="password"
            name="repassword"
            placeholder="******"
            value=""
    >';

//ha létezik a hiba elem, akkor hozzáfűzzük a formhoz
        $form .= getError('repassword');


        $form .= '</label>';


//checkbox - státusz
        /** -A) ki kell pipálni ha:
         * -a) nincs post adat de row adat 1 (azaz van)
         * -b) van post adat és benn van a status elem
         */
//A)
        $checked = '';
        if(
            empty($_POST)//a
            &&
            getValue('status',$row)//a (row alapján)
            ||
            getValue('status') //b //van a postban
        ){
            $checked = 'checked';
        }

        $form .= '<label>
        <span>
        <input type="checkbox" name="status" value="1" ' . $checked . '> Státusz
        </span>';
        $form .= '</label>
<button>Küldés</button>
</form>';

//kiírás 1 lépésben
        $output = $form;
        break;
        //show ág? ahol 1 rekord minden adata látható
    default:
//listázás
        $qry = "SELECT id, name, email, status FROM admins";//kérés kialakítása
        $result = mysqli_query($link, $qry) or die(mysqli_error($link));//lekérés

        $table = '<h2>Adminok lista</h2><div><a href="'.$baseURL.'&amp;action=create" class="btn btn-primary my-2">új felvitel</a></div>
          <table class="table table-bordered table-striped table-responsive">';
//címsor
        $table .= '<tr>
              <th>id</th>
              <th>név</th>
              <th>email</th>
              <th>státusz</th>
              <th>művelet</th>
            </tr>';
        while ($row = mysqli_fetch_row($result)) {
            $table .= '<tr>
                  <td>' . $row[0] . '</td>
                  <td>' . $row[1] . '</td>
                  <td>' . $row[2] . '</td>
                  <td>' . $row[3] . '</td>
                  <td><a href="'.$baseURL.'&amp;action=update&amp;id=' . $row[0] . '">módosít</a> | <a onclick="return confirm(\'Tuti biztos?\')" href="'.$baseURL.'&amp;action=delete&amp;id=' . $row[0] . '">töröl</a></td>
                </tr>';
            //echo '<pre>'.var_export($row,true).'</pre>';//kibontás ciklusban
        }
        $table .= '</table>';
//kiírás 1 lépésben

        $output = $table;
        break;
}
//output kiírás az indexben lesz ahova includeoljuk a modult
//echo $output;//kiírás egy lépésben


//ideiglenes stílusok
$additional_styles = '<style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            display: flex;
            flex-direction: column;
            max-width: 400px;
            margin: 0 auto;
        }

        label {
            display: flex;
            flex-direction: column;
            margin: 5px 0;
        }

        .error {
            color: #f00;
            font-size: 0.8em;
            font-style: italic;
        }
    </style>';
