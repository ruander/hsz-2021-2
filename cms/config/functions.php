<?php
//Saját eljárások gyüjteménye
/**
 * @param $str
 * @return string
 */
function ekezettelenit($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő '-' eltávolítása
    return $str;
}

/**
 * @param $str
 * @return string
 */
function ekezettelenitFileName($str) {
    $bad  = array('á','ä','é','í','ó','ö','ő','ú','ü','ű','Á','Ä','É','Í','Ó','Ö','Ő','Ú','Ü','Ű',' ');
    $good = array('a','a','e','i','o','o','o','u','u','u','a','a','e','i','o','o','o','u','u','u','-');
    $str = mb_strtolower($str,"utf-8");//kisbetűsre
    $str = str_replace($bad, $good, $str);//karakterek cseréje
    $str = preg_replace("/[^A-Za-z0-9_.]/", "-", $str);//maradék eltávolítása
    $str = rtrim($str,'-');//végződő '-' eltávolítása
    return $str;
}
/**
 * kiléptető eljárás
 * @return void
 */
function logout()
{
    global $link;
    //töröljük a sessions rekordot
    mysqli_query($link,"DELETE FROM sessions WHERE sid = '".session_id()."' LIMIT 1") or die(mysqli_error($link));
    $_SESSION = [];//sessions tömb ürítése
    session_destroy();//mf megszüntetése
}

/**
 * Érvényes bélépés ellenőrzése
 * @return bool
 */
function auth(){
    global $link;
    //session_id(),$_SESSION,sesssions tábla rekord,időpont
    $now=time();
    $expired = $now - (60 * 15);//
//töröljük a lejárt bejelentkezéseket
    mysqli_query($link,"DELETE FROM sessions WHERE stime < $expired") or die(mysqli_error($link));
    $sid = session_id();
//kérjük le ha van hozzá rekord
    $qry = "SELECT spass FROM sessions WHERE sid = '$sid' LIMIT 1";
    $result = mysqli_query($link,$qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);//ha van ilyen rekord akkor a  $row[0] lesz a belépéskor gyártott jelszó
//var_dump($row);
    if(
        isset($_SESSION['userdata']['id'])//ha volt belépés, akkor az adatoknak a $_SESSION ben kell lenniük
        &&
        $row[0] === md5($sid.$_SESSION['userdata']['id'].SECRET_KEY)//a most gyártott jelszó és a belépéskor gyártott egyezik (érvényes)
    ){
        //frissítsük az stime-ot
        mysqli_query($link,"UPDATE sessions set stime = $now WHERE sid = '$sid' LIMIT 1") or die(mysqli_error($link));
        return true;//minden oké
    }

    return false;//nincs érvényes bélépés
}

/**
 * Beléptető eljárás :)
 * @return bool
 */
function login()
{
    global $link;//az eljárás 'lássa' a változót
    $email = mysqli_real_escape_string($link, filter_input(INPUT_POST, 'email'));
    $password = filter_input(INPUT_POST, 'password');

//kérjük le a jelszót(és nevet) ha van ilyen email
    $qry = "SELECT name, password, id FROM admins WHERE email = '$email' AND status = 1 LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_row($result);
    //var_dump($row);
//jelszó egyezés ellenőrzése
    if ($row && password_verify($password, $row[1])) {
        //oké
        $sid = $_SESSION['id'] = session_id();
        $_SESSION['userdata'] = [
            'id' => $row[2],
            'name' => $row[0],
            'email' => $email
        ];
        //lastlogin update
        $now = date('Y-m-d H:i:s');
        $qry = "UPDATE admins SET last_login = '$now' WHERE id = '{$row[2]}' LIMIT 1";
        mysqli_query($link, $qry) or die(mysqli_error($link));
        //session id + admin id + SECRET_KEY
        $spass = md5($sid . $row[2] . SECRET_KEY);
        $stime = time();//timestamp (mp alapú)
        var_dump($sid, $spass, $stime);
        //takarítás (beragadt belépések)
        mysqli_query($link, "DELETE FROM sessions WHERE sid = '$sid'") or die(mysqli_error($link));
        //mentés db-be (sessions)
        $qry = "INSERT INTO sessions(sid,spass,stime) VALUES('$sid','$spass','$stime')";
        mysqli_query($link, $qry) or die(mysqli_error($link));
        return true;//minden oké, térjünk vissza true val
    }

    return false;
}

//saját eljárások (helperek)
/**
 * Input mező szöveges valueját adja vissza a POSTból
 * @param $fieldName | input name
 * @return mixed
 */
function getValue($fieldName, $rowData = []){
    //ha van post adat, mivel az a 'legfontosabb' visszatérünk vele
    if( filter_input(INPUT_POST,$fieldName) !== null){
        return filter_input(INPUT_POST,$fieldName);
    }
    //ha nem tértünk vissza post adattal akkor megnézzük van-e elemünk a row-ban
    if(array_key_exists($fieldName,$rowData)){
        return $rowData[$fieldName];
    }

    return '';//üres stringel térünk vissza, ha eddig nem ...
}

/**
 * Hiba kiíró eljárás a korábban vázolt hiba tömbből
 * @param $fieldName
 * @return string
 */
function getError($fieldName){
    global $hiba; //eljárás idejére lássuk a hiba tömböt

    if(isset($hiba[$fieldName])){//ha létezik a hibaüzenete, adjuk vissza
        return $hiba[$fieldName];
    }

    return '';
}

