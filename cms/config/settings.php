<?php

const SECRET_KEY = '!S3cr3T_K3y@';//titkosító 'string' - konstans
const MODULE_DIR = "modules/";//modulok mappája
const MODULE_EXT = ".php";//modulefileok kiterjesztése
/**********/
//admin menü elemek tömbje (később DB-ben is tárolható)
const ADMIN_MENU = [
    0 => [
        'title' => 'Vezérlőpult',
        'icon_class' => 'fas fa-tachometer-alt',
        'module_name' => 'dashboard'
    ],
    1 => [
        'title' => 'Cikkek',
        'icon_class' => 'fa fa-user',
        'module_name' => 'articles'
    ],
    2 => [
        'title' => 'Adminisztrátorok',
        'icon_class' => 'fa fa-user',
        'module_name' => 'admins'
    ],
];
