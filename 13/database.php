<?php
//adatbázi csatlakozási adatok:
$dbHost = 'localhost';
$dbUser = 'root';
$dbPassword = '';
$dbName = 'classicmodels';

//csatlakozás felépítése (resource)
$link = @mysqli_connect($dbHost,$dbUser,$dbPassword,$dbName) or die("GEBASZ:".mysqli_connect_error());
//var_dump($link);

//lekérés összeállítása
$qry = "SELECT * FROM employees";
//lekérés futtatása
$results = mysqli_query($link,$qry) or die(mysqli_error($link));
//eredmények feldolgozása (összes egyszerre)
$rows = mysqli_fetch_all($results,MYSQLI_ASSOC);
echo '<pre>'.var_export($rows,true).'</pre>';
