<?php
//a for ciklus (elöltesztelő)
/*
 for(ciklusváltozó kezdeti értéke; belépési feltétel vizsgálat;ciklusváltozó léptetése){
    //ciklusmag
}
 */
for($i=1;$i<=10;$i++){//operátor: $i++ ($i--);érték növelés(csökkentés) 1-el
    echo "<br>$i";
}

//a while (elöltesztelő)
/*
while(belépési feltétel vizsgálat){
    //ciklusmag
}
 */
$i=1;//'ciklusváltozó kezdeti érték'
while($i<=10){
    echo "<br>$i";
    $i++;//'ciklusváltozó léptetése'
}

/*
do{
    //ciklusmag
}while(feltétel)
 */
$i=1;
do{
    echo "<br>$i";
    $i++;
}while($i<=10);

//Tároljunk el 6 véletlen számot egy tömbben (1-10)
$tomb = [];//ide tároljuk a számokat
for($i=1;$i<=6;$i++){
    $tomb[] = rand(1,10);//generáljunk egy következő indexre egy számot
}

echo '<pre>'.var_export($tomb, true).'</pre>';

//a foreach (elöltesztelő)
/*
foreach($tomb(vagy objektum) AS $key => $value){
    //ciklusmag
    $key és $value elérhető
}
foreach($tomb AS $value){
    //ciklusmag
csak $value érhető el
}
 */
//bejárjuk a fenti tömböt és kiírjuk a kulcsokat-értékeket

foreach($tomb as $k => $v){
    echo "<br>aktuális kulcs: $k | érték: $v";
}

//Tároljunk el 6 véletlen számot egy tömbben (1-10) //while
$tomb = [];//ide tároljuk a számokat
while(count($tomb)<6){
    $tomb[] = rand(1,10);//generáljunk egy következő indexre egy számot
}
echo '<pre>'.var_export($tomb, true).'</pre>';

/**
 * @todo HF: php_alapok_gyakorlas.txt 1-19 kivéve 12.
 */
