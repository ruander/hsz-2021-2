<?php
//Pure PHP file (nem zárjuk sosem a php taget)
echo 'PHP tanfolyam';

//tömbök
/*
[
index(key) => value,
...
]
 */
$tomb = [];//üres tömb létrehozása (régebben: array() )

$tomb = [12, 6/11, true, "Horváth György"];//tömb létrehozása alap értékekkel (primitivek most) automatikus indexre
echo '<pre>'.var_export($tomb, true).'</pre>';
//tömböt nem lehet kiírni, csak a benne található primitiveket
//tömb 1 eleme pl név kiírása (3as index most)
echo $tomb[3];

//automatikus indexre bővítés
$tomb[] = 'Új elem...';
echo '<pre>'.var_export($tomb, true).'</pre>';

//irányított index
$tomb[100] = '100 - index';
echo '<pre>'.var_export($tomb, true).'</pre>';

var_dump( count($tomb) );//tömb elemeinek száma (6)

$tomb[] = "teszt";//101 indexre kerül
echo '<pre>'.var_export($tomb, true).'</pre>';

//asszociatív tömb
$user = [
    'id' => 1,
    'name' => 'George Horváth',
    'email' => 'hgy@iworkshop.hu',
];
echo '<pre>'.var_export($user, true).'</pre>';
//var_dump($user);

//több dimenzió
$user['data'] = $tomb;
echo '<pre>'.var_export($user, true).'</pre>';
//kiírások első ill 2.(n) dimenzióról
echo $user['name'];//angol név
echo '<br>'.$user['data'][3];//magyar név
