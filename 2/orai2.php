<?php
//Elágazások
/*
if(feltétel(ek) (true|false)){
    //igaz ág (true)
}else{
    //hamis ág (false)
}
//******
if(feltétel){
    feltétel igaz
}elseif(feltétel2){
    feltétel nem igaz, feltétel2 igaz
}else{
    feltétel és feltétel2 sem igaz
}
 */
$veletlenSzam = rand(1, 10);
//ha 5nél nagyobb nagy szám, ha 5 vagy kisebb kis szám feliratot írjunk ki és a számot
if ($veletlenSzam > 5) {
    echo "A generált szám: " . $veletlenSzam . ", ami nagy.";
} else {
    echo "A generált szám: " . $veletlenSzam . ", ami kicsi \"szám\".";
}

echo '<div>' . $veletlenSzam . '</div>';
//echo "<div>" . $veletlenSzam . "</div>";
echo '<div>$veletlenSzam</div>';
echo "<div>$veletlenSzam</div>";
echo "<div>\$veletlenSzam</div>";// operátor: \ -> escape , az utána következő karaktert kilépteti a nyelvi elemek közül

/*
switch(vizsgált elem){
    eset 1:
        ..kód
    break;

    eset 2:
        ...kód
    break;
    default:
        ...kód
    break;
}
...
 */
$talalatok = rand(0,5);
switch($talalatok){
    case 5:
        echo 'Csinadratta, telitalálat!';
        break;
    case 4:
        echo '4 találat, sok píz...';
        break;
    case 3:
        echo '3 találat, közepes píz...';
        break;
    case 2:
        echo '2 találat, alamizsna...';
        break;
    default:
        echo 'Sajtos ropogós... nem nyert...';
        break;
}
