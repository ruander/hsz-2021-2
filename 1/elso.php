<!doctype html>
<html lang="hu">
<head>
 <meta charset="utf-8">
 <title>Első PHP kódom</title>
</head>
<body>
Hello World!
<?php
/*
ez egy 
több soros
PHP komment
érdekesség
old:Personal Home Page
PHP Hyper-Text Preprocessor :) -> rekurziv rövidítés
*/
//egysoros komment
//kiírás a standard outputra
echo 'Hello World!';//operátor:  '',"" string határoló | ; -> utasítás lezárása (kötelező)
print "<br>Ez PHP echo eredménye<br>";

//Változók a PHP ban
/*
$valtozoneve : operátor: $ -> változó
$szam
$egeszSzam -> camelCase
$veletlen_szam_1 -> snake_case
$veletlenSzam2
*/
$egeszSzam = 34;//operátor: = -> értékadó operátor , egész szám tipus (integer - int)
$tortSzam = 23/7;//operotorok (matematikai) +,-,*,/, % (maradékos osztás) | tipus: floating point number (float)
$szoveg = "Ez egy <b>szöveg</b>";//tipus: string
$logikai = true;//tipus: boolean vagy bool (true|false)

//fejlesztés közbe info a változókról
echo '<pre>';
var_dump($egeszSzam, $tortSzam, $szoveg, $logikai);//std outputra
echo '</pre>';
//var_export
echo '<pre>';
echo var_export($egeszSzam, true);
echo '</pre>';
//kiírás egy lépésben (konkatenáció) -> . operátor
echo '<pre>' . var_export($tortSzam,true) . '</pre>';
?>
</body>
</html>