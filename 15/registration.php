<?php
//felhasználó (admin/vagy user) felvitele
/**
 * adatbázis: php_tanfolyam
 * tábla: admins
 * [id] - int(10),auto_increment,primary_key,unsigned
 * [name] - varchar(100) utf8_general_ci
 * [email] - varchar(100) utf8_general_ci, unique
 * [password] varchar(100) utf8_general_ci (PASSWORD_BCRYPT)
 * [status] - (boolean) tinyint(1) unsigned
 * [last_login] datetime nullable
 * [time_created] datetime
 * [time_updated] datetime nullable
 * INSERT INTO
  `admins` (`name`, `email`, `password`, `status`, `time_created`)
  VALUES ('superadmin', 'hgy@iworkshop.hu', '$2y$10$bVqZlzEuP4Rck5q..2cng.4LV22GaOfc/aGVTnn80Lzfzzkjjm.My', '1', '2021-06-16 17:31:04');
 */
require "database.php";/** @var $link mysqli */// adatbázis kapcsolat
require "functions.php";//saját eljárások betöltése
//hibakezelés/műveletek (logika)
if (!empty($_POST)) {//csak akkor foglalkozunk a postal ha van benne valami
    echo '<pre>POST: ' . var_export($_POST, true) . '</pre>';
    //adatok feldolgozása
    //hibakezelés
    $hiba = [];//ide gyüjtjük a hibákat
    //név minimum 3 karakter
    $name = filter_input(INPUT_POST, 'name');
    $name = strip_tags($name);//védelem TAGek ellen (script)
    $name = trim($name);//távolítsuk a space-ket
    $name = mysqli_real_escape_string($link,$name);//mysql injection elleni védelem a mezőre
    //var_dump(strlen($name));
    if (mb_strlen($name, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //email kötelező azaz minimum látszódjon emailnek
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    }else{
        //létezik e már?
        $qry = "SELECT id FROM admins WHERE email ='$email' LIMIT 1";

        $result = mysqli_query($link,$qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        //var_dump($row);
        //ha van row, foglalt az email
        if($row !== null){
            $hiba['email'] = '<span class="error">Már regisztrált email!</span>';
        }
    }

    //jelszó
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    //jelszó 1 legalább 6 karakter (mb!)
    if (mb_strlen($password, 'utf-8') < 6) {
        $hiba['password'] = '<span class="error">Legalább 6 karakter kell legyen!</span>';
    } elseif ($password !== $repassword) {//ha nem egyezik a 2 jelszó
        $hiba['repassword'] = '<span class="error">Jelszavak nem egyeztek!</span>';
    } else {
        //jelszó biztonságos kódolása
        $password = password_hash($password, PASSWORD_BCRYPT);
    }


    //státusz
    $status = filter_input(INPUT_POST,'status') ? 1 : 0;//ha ki volt pipálva, 1 egyébként 0


    if (empty($hiba)) {
        //minden ok... adatok rendezése
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'status' => $status
        ];
        //felvitelkor
        $data['time_created'] = date('Y-m-d H:i:s');
        echo '<pre>data: ' . var_export($data, true) . '</pre>';
        //új felvitel az admins táblába
        $qry = "INSERT INTO
  `admins` (`name`, 
            `email`, 
            `password`, 
            `status`, 
            `time_created`)
  VALUES ('{$data['name']}', 
          '{$data['email']}', 
          '{$data['password']}', 
          '{$data['status']}', 
          '{$data['time_created']}');";
        //futtatás
        mysqli_query($link,$qry) or die(mysqli_error($link));
        //átirányítás a listázásra (most admins.php)
        header('location:admins.php');
        exit();
    }

}


//űrlap
$form = '<form method="post">';//form elemek változója
//név
$form .= '<label>
        <span>Név<sup>*</sup></span>
        <input
            type="text"
            name="name"
            placeholder="Gipsz Jakab"
            value="' . getValue('name') . '">';
//ha létezik a hiba elem, akkor hozzáfűzzük a formhoz
$form .= getError('name');
$form .= '</label>';

//email ...
$form .= '<label>
    <span>Email<sup>*</sup></span>
    <input
            type="text"
            name="email"
            placeholder="your@email.com"
            value="' . getValue('email') . '"
    >';

//ha létezik a hiba elem, akkor hozzáfűzzük a formhoz
$form .= getError('email');

$form .= '</label>
<!--Jelszó 1-->
<label>
    <span>Jelszó<sup>*</sup></span>
    <input
            type="password"
            name="password"
            placeholder="******"
            value=""
    >';

//ha létezik a hiba elem, akkor hozzáfűzzük a formhoz
$form .= getError('password');

$form .= '</label>
<!--Jelszó újra-->
<label>
    <span>Jelszó úrja<sup>*</sup></span>
    <input
            type="password"
            name="repassword"
            placeholder="******"
            value=""
    >';

//ha létezik a hiba elem, akkor hozzáfűzzük a formhoz
$form .= getError('repassword');


$form .= '</label>';


$checked = filter_input(INPUT_POST, 'status') ? 'checked' : '';

$form .= '<label>
        <span>
        <input type="checkbox" name="status" value="1" ' . $checked . '> Státusz
        </span>';
$form .= '</label>
<button>Küldés</button>
</form>';

//kiírás 1 lépésben
echo $form;



//ideiglenes stílusok
$styles = '<style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            display: flex;
            flex-direction: column;
            max-width: 400px;
            margin: 0 auto;
        }

        label {
            display: flex;
            flex-direction: column;
            margin: 5px 0;
        }

        .error {
            color: #f00;
            font-size: 0.8em;
            font-style: italic;
        }
    </style>';
echo $styles;
