<?php
//erőforrások
require "database.php";/** @var $link mysqli */// adatbázis kapcsolat
require "functions.php";//saját eljárások betöltése
//url paraméterek
$tid = filter_input(INPUT_GET,'id',FILTER_VALIDATE_INT)?:null;
var_dump($tid);

if (!empty($_POST)) {//csak akkor foglalkozunk a postal ha van benne valami
    echo '<pre>POST: ' . var_export($_POST, true) . '</pre>';
    //adatok feldolgozása
    //hibakezelés
    $hiba = [];//ide gyüjtjük a hibákat
    //név minimum 3 karakter
    $name = filter_input(INPUT_POST, 'name');
    $name = strip_tags($name);//védelem TAGek ellen (script)
    $name = trim($name);//távolítsuk a space-ket
    $name = mysqli_real_escape_string($link,$name);//mysql injection elleni védelem a mezőre
    //var_dump(strlen($name));
    if (mb_strlen($name, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //email kötelező azaz minimum látszódjon emailnek
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    }else{
        //létezik e már?
        $qry = "SELECT id FROM admins WHERE email ='$email' LIMIT 1";

        $result = mysqli_query($link,$qry) or die(mysqli_error($link));
        $row = mysqli_fetch_row($result);
        var_dump($row,$tid);
        //ha van row, foglalt az email
        if($row !== null && $row[0] != $tid){
            $hiba['email'] = '<span class="error">Már regisztrált email!</span>';
        }
    }

    //jelszó
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    //jelszó 1 legalább 6 karakter (mb!) EDIt esetén csak akkor nyulunk a jelszóhoz ha az 1 mező legalább 1 karaktert tartalmaz
    if(strlen($password) > 0) {
        if (mb_strlen($password, 'utf-8') < 6) {
            $hiba['password'] = '<span class="error">Legalább 6 karakter kell legyen!</span>';
        } elseif ($password !== $repassword) {//ha nem egyezik a 2 jelszó
            $hiba['repassword'] = '<span class="error">Jelszavak nem egyeztek!</span>';
        } else {
            //jelszó biztonságos kódolása
            $password = password_hash($password, PASSWORD_BCRYPT);
        }
    }

    //státusz
    $status = filter_input(INPUT_POST,'status') ? 1 : 0;//ha ki volt pipálva, 1 egyébként 0


    if (empty($hiba)) {
        //minden ok... adatok rendezése
        $data = [
            'name' => $name,
            'email' => $email,
            'status' => $status
        ];
        //jelszó csak akkor legyen a data tömbben ha valóban kell módosítani
        if($password){
            $data['password'] = $password;
        }
        //módosításkor
        $data['time_updated'] = date('Y-m-d H:i:s');
        echo '<pre>data: ' . var_export($data, true) . '</pre>';
        //update set query rész a data tömb bejárásával
        $updateElements =  [];
        foreach($data as $fieldName => $value){
            $updateElements[] = "`$fieldName` = '$value'";
        }
        //echo '<pre>data: ' . var_export($updateElements, true) . '</pre>';
        $qry = "UPDATE admins SET ".implode(',',$updateElements)." WHERE id = $tid LIMIT 1";
        //futtatás
        mysqli_query($link,$qry) or die(mysqli_error($link));
        //átirányítás a listázásra (most admins.php)
        header('location:admins.php');
        exit();
    }

}




//sor lekérése
$qry = "SELECT name,email,status FROM admins WHERE id = '$tid' LIMIT 1";
$result = mysqli_query($link,$qry) or die(mysqli_error($link));
$row = mysqli_fetch_assoc($result);
echo '<pre>'.var_export($row,true).'</pre>';
//űrlap
$form = '<form method="post">';//form elemek változója
//név
$form .= '<label>
        <span>Név<sup>*</sup></span>
        <input
            type="text"
            name="name"
            placeholder="Gipsz Jakab"
            value="' . getValue('name', $row) . '">';//db adat, post, vagy üres
//ha létezik a hiba elem, akkor hozzáfűzzük a formhoz
$form .= getError('name');
$form .= '</label>';

//email ...
$form .= '<label>
    <span>Email<sup>*</sup></span>
    <input
            type="text"
            name="email"
            placeholder="your@email.com"
            value="' . getValue('email', $row) . '"
    >';

//ha létezik a hiba elem, akkor hozzáfűzzük a formhoz
$form .= getError('email');

$form .= '</label>
<!--Jelszó 1-->
<label>
    <span>Jelszó<sup>*</sup></span>
    <input
            type="password"
            name="password"
            placeholder="******"
            value=""
    >';

//ha létezik a hiba elem, akkor hozzáfűzzük a formhoz
$form .= getError('password');

$form .= '</label>
<!--Jelszó újra-->
<label>
    <span>Jelszó úrja<sup>*</sup></span>
    <input
            type="password"
            name="repassword"
            placeholder="******"
            value=""
    >';

//ha létezik a hiba elem, akkor hozzáfűzzük a formhoz
$form .= getError('repassword');


$form .= '</label>';


//$checked = filter_input(INPUT_POST, 'status') ? 'checked' : '';
//@todo órai feladat: adatbázis status állapota szerint legyen első betöltéskor kipipálva, de
//ha van post és benne van a status akkor mindenképp ki kell pipálni
//ha van post de nincs benne státusz akkor mindenképpen üresen kell hagyni
//checkbox - státusz
/** -A) ki kell pipálni ha:
 * -a) nincs post adat de row adat 1 (azaz van)
 * -b) van post adat és benn van a status elem
 *
 * -B) nem kell kipipálni ha
 * -a)nincs semmi (se row adat se post adat)
 * -b) van row adat (0) van post de nincs benne az elem
 */
//A)
$checked = '';
if(
    empty($_POST)//a
    &&
    getValue('status',$row)//a (row alapján)
    ||
    getValue('status') //b //van a postban
){
    $checked = 'checked';
}
//B
/*$checked = 'checked';
if(
    !empty($_POST)//b
    &&
    !filter_input(INPUT_POST,'status')
    ||
    !getValue('status',$row)
){
    $checked = '';
}*/

$form .= '<label>
        <span>
        <input type="checkbox" name="status" value="1" ' . $checked . '> Státusz
        </span>';
$form .= '</label>
<button>Küldés</button>
</form>';

//kiírás 1 lépésben
echo $form;



//ideiglenes stílusok
$styles = '<style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            display: flex;
            flex-direction: column;
            max-width: 400px;
            margin: 0 auto;
        }

        label {
            display: flex;
            flex-direction: column;
            margin: 5px 0;
        }

        .error {
            color: #f00;
            font-size: 0.8em;
            font-style: italic;
        }
    </style>';
echo $styles;
