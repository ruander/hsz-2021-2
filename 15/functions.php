<?php
//saját eljárások (helperek)
/**
 * Input mező szöveges valueját adja vissza a POSTból
 * @param $fieldName | input name
 * @return mixed
 */
function getValue($fieldName, $rowData = []){
    //ha van post adat, mivel az a 'legfontosabb' visszatérünk vele
    if( filter_input(INPUT_POST,$fieldName) !== null){
        return filter_input(INPUT_POST,$fieldName);
    }
    //ha nem tértünk vissza post adattal akkor megnézzük van-e elemünk a row-ban
    if(array_key_exists($fieldName,$rowData)){
        return $rowData[$fieldName];
    }

    return '';//üres stringel térünk vissza, ha eddig nem ...
}

/**
 * Hiba kiíró eljárás a korábban vázolt hiba tömbből
 * @param $fieldName
 * @return string
 */
function getError($fieldName){
    global $hiba; //eljárás idejére lássuk a hiba tömböt

    if(isset($hiba[$fieldName])){//ha létezik a hibaüzenete, adjuk vissza
        return $hiba[$fieldName];
    }

    return '';
}

