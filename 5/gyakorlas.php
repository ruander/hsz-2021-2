<?php
/*
alapfeladatok TXT fileból
 */


//15.Írjon egy programot, amely kiszámolja az 1-től 20-ig terjedő egész számok négyzetét, de csak azokat az értékeket írja ki, amely nagyobb, mint 30. Használjon ciklus utasítást.

for($i=1;$i<=20;$i++){

    $negyzet = pow($i,2);
    if($negyzet > 30){
        echo "<div>i értéke: $i, négyzete: $negyzet</div>";
    }

}

//16.Írjon egy programot, amely kiszámolja az 1*3*5*7*9*11*13*15*17*19 értéket ciklus utasítás segítségével.
$mtpl = 1;
for($i=1;$i<20; $i+=2){
    //echo "<br>$i";
    $mtpl *= $i;
}
echo "<div>1*3*5*7*9*11*13*15*17*19 = $mtpl</div>";

///17.Írjon egy programot, amelyben egy 20 elemű tetszőleges tömböt hoz létre és kiírja a tömb minden második elemét.
//ciklusban generálunk egy tömbbe véletlen számokat 1-1000
$tomb=[];
for($i=1;$i<=20;$i++){
    $tomb[] = rand(1,1000);
}
echo '<pre>'.var_export($tomb,true).'</pre>';
//kiírjuk mminden 2. elemét
$i=1;
foreach($tomb as $k => $v){
    //segédváltozó vizsgálata
    if($i%2 == 0){
        echo "<br>kulcs: $k , érték: $v";
    }
    $i++;//segédváltozó léptetése
}


//PDF feladatgyüjtemény
//6. Készítsen egy olyan ciklust, ami egy fejléces táblázatot ír ki.
$table = '<table border="1">';
$fejlec = '<tr>
                <th>id</th>
                <th>name</th>
                <th>email</th>
                <th>statusz</th>
                <th>tel</th>
           </tr>';//ez egy fejléc sor
$headline = 1;//nincs fejléc
//ciklus a soroknak
for($i=1;$i<=100;$i++){
    //ha még nincs fejléc, akkor beillesztjük
    if($headline === 1 || $headline%25 === 0){
        $table .= $fejlec;
    }
    $headline++;//léptetjük a fejléc segédet
    $table .= '<tr>
                <td>'.$i.'</td>
                <td>adat</td>
                <td>adat</td>
                <td>adat</td>
                <td>adat</td>
           </tr>';//ez egy fejléc sor
}


$table .= '</table>';
//táblázat kiírása 1 lépésben
echo $table;
