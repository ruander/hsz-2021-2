<?php
//erőforrások

const SECRET_KEY = '!S3cr3T_K3y@';//titkosító 'string' - konstans

//a $_POST szuperglobális tömb

if (!empty($_POST)) {//csak akkor foglalkozunk a postal ha van benne valami
    echo '<pre>POST: ' . var_export($_POST, true) . '</pre>';
    //adatok feldolgozása
    //hibakezelés
    $hiba = [];//ide gyüjtjük a hibákat
    //név minimum 3 karakter
    $name = filter_input(INPUT_POST, 'name');//@todo: HF _átnézni jól: https://www.php.net/manual/en/function.filter-input.php, https://www.php.net/manual/en/filter.filters.validate.php

    $name = strip_tags($name);//védelem TAGek ellen (script)

    $name = trim($name);//távolítsuk a space-ket

    //var_dump(strlen($name));
    if (mb_strlen($name, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //email kötelező azaz minimum látszódjon emailnek
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    }

    //jelszó
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    //jelszó 1 legalább 6 karakter (mb!)
    if (mb_strlen($password, 'utf-8') < 6) {
        $hiba['password'] = '<span class="error">Legalább 6 karakter kell legyen!</span>';
    } elseif ($password !== $repassword) {//ha nem egyezik a 2 jelszó
        $hiba['repassword'] = '<span class="error">Jelszavak nem egyeztek!</span>';
    } else {
        //a jelszó jó formátumú azaz min 6 karakter és egyeztek

        //elkódoljuk, most md5tel - mostmár SECRET_KEY hozzáadásával
        //$password = md5($password.SECRET_KEY);//hash
        //jelszó biztonságos kódolása
        $password = password_hash($password, PASSWORD_BCRYPT);
    }


    //terms kötelező elfogadása
    if (!filter_input(INPUT_POST, 'terms')) {
        $hiba['terms'] = '<span class="error">Kötelező kipipálni!</span>';
    }


    if (empty($hiba)) {
        //minden ok... adatok rendezése
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password
        ];
        echo '<pre>data: ' . var_export($data, true) . '</pre>';
        die();//állj
    }

}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap feldolgozás azonos file-ban</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            display: flex;
            flex-direction: column;
            max-width: 400px;
            margin: 0 auto;
        }

        label {
            display: flex;
            flex-direction: column;
            margin: 5px 0;
        }

        .error {
            color: #f00;
            font-size: 0.8em;
            font-style: italic;
        }
    </style>
</head>
<body>
<form method="post">
    <label>
        <span>Név<sup>*</sup></span>
        <input
                type="text"
                name="name"
                placeholder="Gipsz Jakab"
                value="<?php echo filter_input(INPUT_POST, 'name'); ?>"
        >
        <?php
        //ha létezik a hiba elem, akkor kiírjuk
        if (isset($hiba['name'])) {
            echo $hiba['name'];
        }
        ?>
    </label>
    <label>
        <span>Email<sup>*</sup></span>
        <input
                type="text"
                name="email"
                placeholder="your@email.com"
                value="<?php echo filter_input(INPUT_POST, 'email'); ?>"
        >
        <?php
        //ha létezik a hiba elem, akkor kiírjuk
        if (isset($hiba['email'])) {
            echo $hiba['email'];
        }
        ?>
    </label>
    <!--Jelszó 1-->
    <label>
        <span>Jelszó<sup>*</sup></span>
        <input
                type="password"
                name="password"
                placeholder="******"
                value=""
        >
        <?php
        //ha létezik a hiba elem, akkor kiírjuk
        if (isset($hiba['password'])) {
            echo $hiba['password'];
        }
        ?>
    </label>
    <!--Jelszó újra-->
    <label>
        <span>Jelszó úrja<sup>*</sup></span>
        <input
                type="password"
                name="repassword"
                placeholder="******"
                value=""
        >
        <?php
        //ha létezik a hiba elem, akkor kiírjuk
        if (isset($hiba['repassword'])) {
            echo $hiba['repassword'];
        }
        ?>
    </label>

    <?php
    /*$checked = '';//alapállapota, ne legyen kipipálva
    //ha ki volt pipálva, akkor szűrhető a POSTból, és a 'checked' értéket kell beállítanunk
    //var_dump(filter_input(INPUT_POST,'terms'));
    if(filter_input(INPUT_POST,'terms')!* !== null*!){
        $checked = 'checked';
    }*/
    /*
     * Short if
    if(condition){
            $a = 1;
    }else{
            $a = 10;
    }
    $a = condition ? true : false
    $a = condition ? 1 : 10;
     * */
    $checked = filter_input(INPUT_POST, 'terms') ? 'checked' : '';
    ?>
    <label>
        <span>
        <input type="checkbox" name="terms" value="1" <?php echo $checked; ?>> Elolvastam és megértettem az <a
                    href="#terms" target="_blank">adatvédelmi tájékoztató</a>t!
        </span>
        <?php
        //ha létezik a hiba elem, akkor kiírjuk
        if (isset($hiba['terms'])) {
            echo $hiba['terms'];
        }
        ?>
    </label>
    <button>Küldés</button>
</form>
</body>
</html>
