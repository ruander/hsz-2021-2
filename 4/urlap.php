<?php

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Űrlap feldolgozása</title>
</head>
<body>
<h1>Ürlap feldolgozás 1</h1>
<h2>GET</h2>
<form method="get" action="feldolgoz.php">
    <label>
        <span>teszt</span>
        <input type="text" name="teszt" placeholder="123" value="">
    </label>
    <!--<input type="submit" name="submit" value="küldés">-->
    <button>küldés</button>
</form>
<h2>POST</h2>
<form method="post" action="feldolgoz.php?teszt=hello">
    <label>
        <span>teszt</span>
        <input type="text" name="teszt" placeholder="123" value="">
    </label>

    <button>küldés</button>
</form>
</body>
</html>
