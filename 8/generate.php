<?php
//ez a file generálja a véletlen számokat
require "../9/functions.php";
require "settings.php";
$qty_numbers = filter_input(INPUT_GET,'jatektipus',FILTER_VALIDATE_INT)?:5;

$limit = VALID_GAME_TYPES[$qty_numbers];
$generatedNumbers = generateNumbers($qty_numbers,$limit);
echo json_encode($generatedNumbers);

