<?php
require "../9/functions.php";//generateNumbers()
require  "settings.php";//VALID_GAME_TYPES
$huzasok_szama = filter_input(INPUT_GET,'jatektipus',FILTER_VALIDATE_INT)?:5;
if (!array_key_exists($huzasok_szama, VALID_GAME_TYPES)) {
    die('Gáz van!');
}
$limit = VALID_GAME_TYPES[$huzasok_szama];
//mappanév kialakítása
$year = date('Y');
$week = date('W');
$dir = "tippek/$year/$huzasok_szama/";//ebbe a mappába dolgozunk
if (!is_dir($dir)) {
    mkdir($dir, 0755, true);
}
$data = [
    'name' => 'Test User',
    'email' => 'test@example.com',
    'tippek' => generateNumbers($huzasok_szama,$limit)
];
$now = date('Y-m-d H:i:s');
//eltároljuk az adatok közé az időpontot
$data['time_created'] = $now;

///a megfelelő mappába ($dir) kiírjuk az adatokat növekményes módon
$fileName = $week . '.json';
//beolvasod ha vannak tippek egy tömbbe
if (file_exists($dir . $fileName)) {
    $oldTips = json_decode(file_get_contents($dir . $fileName), true);
} else {//ha nincsenek, akkor üres tömb
    $oldTips = [];
}

//hozzáadod a mostani adatokat
//$oldTips[] = $data; //vagy
array_push($oldTips, $data);
//jsonné alakítod
$dataToFile = json_encode($oldTips);
//kiírod file-ba
file_put_contents($dir . $fileName, $dataToFile);
echo '<pre>'.var_export($data, true).'</pre>';

