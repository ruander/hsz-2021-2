<?php
require "settings.php";//VALID_GAME_TYPES
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lottójáték</title>
</head>
<body>
<h1>Válassz játéktípust!</h1>
<ul>
    <?php
    foreach (VALID_GAME_TYPES as $hsz => $limit) {
        echo '<li><a href="lottojatek.php?jatektipus=' . $hsz . '">' . $hsz . '/' . $limit . ' Játék</a></li>';
    }
    ?>
</ul>
</body>
</html>
