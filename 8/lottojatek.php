<?php
require "settings.php";//VALID_GAME_TYPES
//adjunk lehetőséget url paraméterből vezérelni a folyamatot (pl:jatektipus=5 -> huzasok_szama=5)
$huzasok_szama = filter_input(INPUT_GET, 'jatektipus', FILTER_VALIDATE_INT) ?: 5;//ennyi tipp kell

//ellenőrizni kell hogy a kapott jatektipus érték érvényes játéktipus-e? , mert ha nem , akkor vagy die(hibaüzenet),
if (!array_key_exists($huzasok_szama, VALID_GAME_TYPES)) {
    header('location:index.php');
    exit();
}
// vagy visszairányítod a folyamatot egy biztosan jó értékkel (jatektipus=5)
// vagy beállítod egy érvényes értékre és azzal viszed tovább a folyamatot (url-ben ilyenkor a rossz érték marad, az űrlap az egyik tipus lesz)
//@todo HF extra, a PDF-ben szereplő folyamatábra teljes megvalósítása, választható játéktípussal
$limit = VALID_GAME_TYPES[$huzasok_szama];//1 és limit közé kell esnie a tippeknek
//mappanév kialakítása
$year = date('Y');
$week = date('W');
$dir = "tippek/$year/$huzasok_szama/";//ebbe a mappába dolgozunk
if (!is_dir($dir)) {
    mkdir($dir, 0755, true);
}

if (!empty($_POST)) {//csak akkor foglalkozunk a postal ha van benne valami
    echo '<pre>POST: ' . var_export($_POST, true) . '</pre>';
    //adatok feldolgozása
    //hibakezelés
    $hiba = [];//ide gyüjtjük a hibákat
    //név minimum 3 karakter
    $name = trim(strip_tags(filter_input(INPUT_POST, 'name')));

    //var_dump(strlen($name));
    if (mb_strlen($name, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //email kötelező azaz minimum látszódjon emailnek
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    }
    //@todo tippek hibakezelése
    /*
         // Egy lépésben hibakezelve az összes tipp mező
        $options = [
            'options' => [
                'min_range' => 1,
                'max_range' => $limit
            ],
            'flags' => FILTER_REQUIRE_ARRAY
        ];
        $tippek = filter_input(INPUT_POST, 'tippek', FILTER_VALIDATE_INT, $options);
        foreach($tippek as $k=>$tipp){
            if(!$tipp){
                $hiba['tippek'][$k] = '<span class="error">Hibás formátum!</span>';
            }
        }*/

    /*
     * külön filter és értékvizsgálattal
     */
    $tippek = filter_input(INPUT_POST, 'tippek', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
    echo '<pre>tippek: ' . var_export($tippek, true) . '</pre>';
    $egyedi_tippek = array_unique($tippek);//csak egyedi értékek maradnak, kulcsok nem változnak
    echo '<pre>egyedi tippek: ' . var_export($egyedi_tippek, true) . '</pre>';
    foreach ($tippek as $k => $tipp) {
        if ($tipp < 1 || $tipp > $limit) {
            $hiba['tippek'][$k] = '<span class="error">Hibás formátum!</span>';
        } elseif (!array_key_exists($k, $egyedi_tippek)) {//ha a kulcsa ($k) nincs az egyedi tippek tömbben akkor ismétlődés miatt kikerült az elem a tömbből
            $hiba['tippek'][$k] = '<span class="error">Már szerepelt!</span>';
        }


    }
    echo '<pre>hiba: ' . var_export($hiba, true) . '</pre>';
//terms kötelező elfogadása
    if (!filter_input(INPUT_POST, 'terms')) {
        $hiba['terms'] = '<span class="error">Kötelező kipipálni!</span>';
    }
    if (empty($hiba)) {
        //minden ok... adatok rendezése
        sort($tippek);//emelkedő sorrend
        $data = [
            'name' => $name,
            'email' => $email,
            'tippek' => $tippek
        ];
        $now = date('Y-m-d H:i:s');
        //eltároljuk az adatok közé az időpontot
        $data['time_created'] = $now;
        echo '<pre>data: ' . var_export($data, true) . '</pre>';
        ///a megfelelő mappába ($dir) kiírjuk az adatokat növekményes módon
        $fileName = $week . '.json';
        //beolvasod ha vannak tippek egy tömbbe
        if (file_exists($dir . $fileName)) {
            $oldTips = json_decode(file_get_contents($dir . $fileName), true);
        } else {//ha nincsenek, akkor üres tömb
            $oldTips = [];
        }

        //hozzáadod a mostani adatokat
        //$oldTips[] = $data; //vagy
        array_push($oldTips, $data);
        //jsonné alakítod
        $dataToFile = json_encode($oldTips);
        //kiírod file-ba
        file_put_contents($dir . $fileName, $dataToFile);
        //visszairányatasz az üres form-ra
        header('location:' . $_SERVER['PHP_SELF']);
        die();//állj
    }

}

//PURE (raw) PHP form
$form = '<form method="post" class="tippek-form">';//form elemek változója
//név
$form .= '<label>
        <span>Név<sup>*</sup></span>
        <input
            type="text"
            name="name"
            placeholder="Gipsz Jakab"
            value="' . filter_input(INPUT_POST, 'name') . '">';
//hiba hozzáfűzése a formhoz (label zárás előtt)
if (isset($hiba['name'])) {
    $form .= $hiba['name'];
}
$form .= '</label>';

//email ...
$form .= '<label>
    <span>Email<sup>*</sup></span>
    <input
            type="text"
            name="email"
            placeholder="your@email.com"
            value="' . filter_input(INPUT_POST, 'email') . '"
    >';

//ha létezik a hiba elem, akkor kiírjuk
if (isset($hiba['email'])) {
    $form .= $hiba['email'];
}
$form .= '</label>';
//tippgenerátor
$form .= '<div><span id="generator">gépi szelvény</span><span id="tippInfo"></span></div>';
//ciklus a tippeknek
//ha vannak már tippek kivesszük 1 tömbbe
$oldValues = filter_input(INPUT_POST, 'tippek', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
for ($i = 1; $i <= $huzasok_szama; $i++) {
    $form .= '<label>
        <span>Tipp ' . $i . '<sup>*</sup></span>
        <input
                type="text"
                name="tippek[' . $i . ']"
                placeholder="1-' . $limit . '"
                value="' . (isset($oldValues[$i]) ? $oldValues[$i] : '') . '"
        >';//érték visszaírása a ciklus előtt kialakított segédtömbből, ahol minden érték jelen van, a rosszak is a default filter miatt
//hiba hozzáfűzése a formhoz (label zárás előtt)
    if (isset($hiba['tippek'][$i])) {
        $form .= $hiba['tippek'][$i];
    }
    $form .= '</label>';
}

$checked = filter_input(INPUT_POST, 'terms') ? 'checked' : '';

$form .= '<label>
        <span>
        <input type="checkbox" name="terms" value="1" ' . $checked . '> Elolvastam és megértettem az <a
                    href="#terms" target="_blank">adatvédelmi tájékoztató</a>t!
        </span>';

//ha létezik a hiba elem, akkor kiírjuk
if (isset($hiba['terms'])) {
    $form .= $hiba['terms'];
}
$form .= '</label>
<button>Küldés</button>
</form>';
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,  initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>LottóJáték (<?php echo "$huzasok_szama/$limit" ?>)</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<section>
    <h1>Lottójáték (<?php echo "$huzasok_szama/$limit" ?>) - adja meg a tippjeit! - <a href="index.php">vissza a játéktipusokhoz</a></h1>
    <?php
    //űrlap kiírása
    echo $form;
    ?>
</section>
<script>
    let generatorBtn = document.getElementById('generator');
    let tippInfo = document.getElementById('tippInfo');//üzenet doboz
    tippInfo.innerHTML = '';//tartalom törlése
    generatorBtn.onclick = function () {
        //console.log('megnyomtad');
        tippInfo.innerHTML = '';//tartalom törlése
        loadTippek(<?php echo $huzasok_szama; ?>);
    }

    function loadTippek(qty_numbers = 5){
        let xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if(this.readyState == 4 && this.status == 200){
                //console.log(this.responseText)

                let tippek = JSON.parse(this.responseText);//a php jsonben adja vissza ezért visszaalakitjuk tömbbé
                //tegyük a html input dobozokba
                let elem;
                for(let i=1;i<=tippek.length;i++){
                    elem = document.querySelector('[name="tippek['+i+']"]');
                    elem.value = tippek[i-1];
                }
                tippInfo.innerHTML = 'Tippek kitöltve (' + tippek +')';//tartalom törlése
                //console.log(tippek)
            }
        }

        xhttp.open('GET','generate.php?jatektipus=' + qty_numbers,true);
        xhttp.send();
    }
</script>
</body>
</html>
