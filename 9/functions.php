<?php
//saját eljárások 'gyüjteménye' (helpers)

/**
 * Véletlen számok generálása a kivánt határok között (limit) a kívánt mennyiségben (qty_numbers)
 * @param int $qty_numbers
 * @param int $limit
 * @return array
 */
function generateNumbers($qty_numbers = 5, $limit = 90){
    //paraméterek ellenőrzése, hogy ne fussunk végtelen ciklusba
    if(
        $qty_numbers>$limit //ha  a kért számok mennyisége nagyobb mint a limit
        || !is_int($qty_numbers) //VAGY nem egész számot kaptunk qty_numbers-re
        || !is_int($limit) //VAGY nem egész számot kaptunk limit-re
    ){
        trigger_error('Hibás paraméterezés!!!!!',E_USER_ERROR);
        //return false;
    }
    $generatedNumbers = [];
    while(count($generatedNumbers)< $qty_numbers){
        $generatedNumbers[] = rand(1,$limit);
        $generatedNumbers = array_unique($generatedNumbers);
    }
    sort($generatedNumbers);

    //visszatérünk a kívánt elemmel
    return $generatedNumbers;
}
