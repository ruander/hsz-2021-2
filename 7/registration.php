<?php
//erőforrások
const SECRET_KEY = '!S3cr3T_K3y@';//titkosító 'string' - konstans
$dir = 'data/';//ebbe a mappába dolgozunk
if(!is_dir($dir)){
    mkdir($dir,0755,true);
}

if (!empty($_POST)) {//csak akkor foglalkozunk a postal ha van benne valami
    echo '<pre>POST: ' . var_export($_POST, true) . '</pre>';
    //adatok feldolgozása
    //hibakezelés
    $hiba = [];//ide gyüjtjük a hibákat
    //név minimum 3 karakter
    $name = trim(strip_tags(filter_input(INPUT_POST, 'name')));

    //var_dump(strlen($name));
    if (mb_strlen($name, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //email kötelező azaz minimum látszódjon emailnek
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    }

    //jelszó
    $password = filter_input(INPUT_POST, 'password');
    $repassword = filter_input(INPUT_POST, 'repassword');
    //jelszó 1 legalább 6 karakter (mb!)
    if (mb_strlen($password, 'utf-8') < 6) {
        $hiba['password'] = '<span class="error">Legalább 6 karakter kell legyen!</span>';
    } elseif ($password !== $repassword) {//ha nem egyezik a 2 jelszó
        $hiba['repassword'] = '<span class="error">Jelszavak nem egyeztek!</span>';
    } else {
        //jelszó biztonságos kódolása
        $password = password_hash($password, PASSWORD_BCRYPT);
    }

    //terms kötelező elfogadása
    if (!filter_input(INPUT_POST, 'terms')) {
        $hiba['terms'] = '<span class="error">Kötelező kipipálni!</span>';
    }


    if (empty($hiba)) {
        //minden ok... adatok rendezése
        $data = [
            'name' => $name,
            'email' => $email,
            'password' => $password
        ];
        echo '<pre>data: ' . var_export($data, true) . '</pre>';

        $fileName = 'users.json';
        /**
         * file_exists()
         * 1. ha létezik már a file, olvassuk be és alakítsuk tömbbé ($users)
         *
         * 2. ha nem létezik ilyen file, hozzunk létre egy üres tömböt ($users),
         *
         * 3. majd adjuk hozzá a jelenlegi user adathalmazát egy ujabb elemnek ($users[])
         *
         * 4. alakítsuk vissza kiírható formátumba, és írjuk ki a tartalmat
         *
         * 5. (ha minden oké) utána irányíts át az üres formra
         */
        if(file_exists($dir . $fileName)){// 1.
            $readData = file_get_contents($dir . $fileName);
            $users = json_decode($readData,true);
        }else {// 2.
            $users=[];
        }
        $users[] = $data;//3.
        $dataToFile = json_encode($users); //4.a
        file_put_contents($dir . $fileName, $dataToFile);//4.b
        header('location:'.$_SERVER['PHP_SELF']);//5.
        //        var_dump($users);
        die();//állj
    }

}

?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <title>Regisztrációs űrlap - file, json</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            display: flex;
            flex-direction: column;
            max-width: 400px;
            margin: 0 auto;
        }

        label {
            display: flex;
            flex-direction: column;
            margin: 5px 0;
        }

        .error {
            color: #f00;
            font-size: 0.8em;
            font-style: italic;
        }
    </style>
</head>
<body>
<?php
//PURE (raw) PHP form
$form = '<form method="post">';//form elemek változója
//név
$form .= '<label>
        <span>Név<sup>*</sup></span>
        <input
            type="text"
            name="name"
            placeholder="Gipsz Jakab"
            value="' . filter_input(INPUT_POST, 'name') . '">';
//hiba hozzáfűzése a formhoz (label zárás előtt)
if (isset($hiba['name'])) {
    $form .= $hiba['name'];
}
$form .= '</label>';

//email ...
$form .= '<label>
    <span>Email<sup>*</sup></span>
    <input
            type="text"
            name="email"
            placeholder="your@email.com"
            value="' . filter_input(INPUT_POST, 'email') . '"
    >';

//ha létezik a hiba elem, akkor kiírjuk
if (isset($hiba['email'])) {
    $form .= $hiba['email'];
}

$form .= '</label>
<!--Jelszó 1-->
<label>
    <span>Jelszó<sup>*</sup></span>
    <input
            type="password"
            name="password"
            placeholder="******"
            value=""
    >';

//ha létezik a hiba elem, akkor kiírjuk
if (isset($hiba['password'])) {
    $form .= $hiba['password'];
}

$form .= '</label>
<!--Jelszó újra-->
<label>
    <span>Jelszó úrja<sup>*</sup></span>
    <input
            type="password"
            name="repassword"
            placeholder="******"
            value=""
    >';

//ha létezik a hiba elem, akkor kiírjuk
if (isset($hiba['repassword'])) {
    $form .= $hiba['repassword'];
}

$form .= '</label>';


$checked = filter_input(INPUT_POST, 'terms') ? 'checked' : '';

$form .= '<label>
        <span>
        <input type="checkbox" name="terms" value="1" ' . $checked . '> Elolvastam és megértettem az <a
                    href="#terms" target="_blank">adatvédelmi tájékoztató</a>t!
        </span>';

//ha létezik a hiba elem, akkor kiírjuk
if (isset($hiba['terms'])) {
    $form .= $hiba['terms'];
}
$form .= '</label>
<button>Küldés</button>
</form>';

//kiírás 1 lépésben
echo $form;

?>
</body>
</html>
