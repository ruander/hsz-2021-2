<?php
//erőforrások
$huzasok_szama = 5;//ennyi tipp kell
$limit = 90;//1 és limit közé kell esnie a tippeknek

$dir = 'tippek/';//ebbe a mappába dolgozunk
if (!is_dir($dir)) {
    mkdir($dir, 0755, true);
}

if (!empty($_POST)) {//csak akkor foglalkozunk a postal ha van benne valami
    echo '<pre>POST: ' . var_export($_POST, true) . '</pre>';
    //adatok feldolgozása
    //hibakezelés
    $hiba = [];//ide gyüjtjük a hibákat
    //név minimum 3 karakter
    $name = trim(strip_tags(filter_input(INPUT_POST, 'name')));

    //var_dump(strlen($name));
    if (mb_strlen($name, "utf-8") < 3) {
        $hiba['name'] = '<span class="error">Legalább 3 karakter!</span>';
    }
    //email kötelező azaz minimum látszódjon emailnek
    $email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
    //var_dump($email);
    if (!$email) {
        $hiba['email'] = '<span class="error">Hibás formátum!</span>';
    }
    //@todo tippek hibakezelése
/*
     // Egy lépésben hibakezelve az összes tipp mező
    $options = [
        'options' => [
            'min_range' => 1,
            'max_range' => $limit
        ],
        'flags' => FILTER_REQUIRE_ARRAY
    ];
    $tippek = filter_input(INPUT_POST, 'tippek', FILTER_VALIDATE_INT, $options);
    foreach($tippek as $k=>$tipp){
        if(!$tipp){
            $hiba['tippek'][$k] = '<span class="error">Hibás formátum!</span>';
        }
    }*/

    /*
     * külön filter és értékvizsgálattal
     */
    $tippek = filter_input(INPUT_POST,'tippek',FILTER_DEFAULT,FILTER_REQUIRE_ARRAY);
    echo '<pre>tippek: ' . var_export($tippek, true) . '</pre>';
    foreach($tippek as $k => $tipp){
        if($tipp < 1 || $tipp > $limit){
            $hiba['tippek'][$k] = '<span class="error">Hibás formátum!</span>';
        }

    }
    echo '<pre>hiba: ' . var_export($hiba, true) . '</pre>';
//terms kötelező elfogadása
    if (!filter_input(INPUT_POST, 'terms')) {
        $hiba['terms'] = '<span class="error">Kötelező kipipálni!</span>';
    }
    if (empty($hiba)) {
        //minden ok... adatok rendezése
        $data = [
            'name' => $name,
            'email' => $email
        ];
        echo '<pre>data: ' . var_export($data, true) . '</pre>';

        die();//állj
    }

}

//PURE (raw) PHP form
$form = '<form method="post">';//form elemek változója
//név
$form .= '<label>
        <span>Név<sup>*</sup></span>
        <input
            type="text"
            name="name"
            placeholder="Gipsz Jakab"
            value="' . filter_input(INPUT_POST, 'name') . '">';
//hiba hozzáfűzése a formhoz (label zárás előtt)
if (isset($hiba['name'])) {
    $form .= $hiba['name'];
}
$form .= '</label>';

//email ...
$form .= '<label>
    <span>Email<sup>*</sup></span>
    <input
            type="text"
            name="email"
            placeholder="your@email.com"
            value="' . filter_input(INPUT_POST, 'email') . '"
    >';

//ha létezik a hiba elem, akkor kiírjuk
if (isset($hiba['email'])) {
    $form .= $hiba['email'];
}
//ciklus a tippeknek
//ha vannak már tippek kivesszük 1 tömbbe
$oldValues = filter_input(INPUT_POST,'tippek',FILTER_DEFAULT,FILTER_REQUIRE_ARRAY);
for ($i = 1; $i <= $huzasok_szama; $i++) {
    $form .= '<label>
        <span>Tipp ' . $i . '<sup>*</sup></span>
        <input
                type="text"
                name="tippek[' . $i . ']"
                placeholder="1-' . $limit . '"
                value="' . ( isset($oldValues[$i]) ? $oldValues[$i] : '') . '"
        >';//érték visszaírása a ciklus előtt kialakított segédtömbből, ahol minden érték jelen van, a rosszak is a default filter miatt
//hiba hozzáfűzése a formhoz (label zárás előtt)
    if (isset($hiba['tippek'][$i])) {
        $form .= $hiba['tippek'][$i];
    }
    $form .= '</label>';
}

$checked = filter_input(INPUT_POST, 'terms') ? 'checked' : '';

$form .= '<label>
        <span>
        <input type="checkbox" name="terms" value="1" ' . $checked . '> Elolvastam és megértettem az <a
                    href="#terms" target="_blank">adatvédelmi tájékoztató</a>t!
        </span>';

//ha létezik a hiba elem, akkor kiírjuk
if (isset($hiba['terms'])) {
    $form .= $hiba['terms'];
}
$form .= '</label>
<button>Küldés</button>
</form>';

//kiírás 1 lépésben
echo $form;
