<?php
require "database.php";//adatbázis kapcsolat betöltése
//lekérés összeállítása
$qry = "SELECT firstname fn,lastname ln,employeenumber FROM employees ";
$result = mysqli_query($link,$qry) or die(mysqli_error($link));//lekérés futtatása
//eredmény kibontása 1 sor
$row = mysqli_fetch_row($result);
echo '<pre>'.var_export($row,true).'</pre>';
echo '<div>A név: <b>'.$row[0].' '.$row[1].'</b></div>';//név kiírás minta
//eredmény kibontása 1 sor
$row = mysqli_fetch_assoc($result);
echo '<pre>'.var_export($row,true).'</pre>';
echo '<div>A név: <b>'.$row['fn'].' '.$row['ln'].'</b></div>';//név kiírás minta
//eredmény kibontása 1 sor
$row = mysqli_fetch_array($result);
echo '<pre>'.var_export($row,true).'</pre>';
echo '<div>A név: <b>'.$row['fn'].' '.$row[1].'</b></div>';//név kiírás minta

//eredmény kibontása 1 sor
$row = mysqli_fetch_object($result);
echo '<pre>'.var_export($row,true).'</pre>';
echo '<div>A név: <b>'.$row->fn.' '.$row->ln.'</b></div>';//név kiírás minta

//eredmények kibontása ciklusban
while(null !== $row = mysqli_fetch_assoc($result)){
    echo '<pre>'.var_export($row,true).'</pre>';
    echo '<div>A név: <b>'.$row['fn'].' '.$row['ln'].'</b></div>';//név kiírás
}
echo 'nevek kiírásának vége';

