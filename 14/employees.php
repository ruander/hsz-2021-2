<?php
require "database.php";
//url paraméter kinyerése ha van, vagy null/false
$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
$action = filter_input(INPUT_GET,'action')?:'list';//ez a paraméter mondja meg mit szeretnénk csinálni
$output = '';
//var_dump($action,$id);
switch ($action){
    case 'show':
        //ha van id-nk akkor kérjük le az adatokat
        if ($id) {
            $qry = "SELECT * FROM employees WHERE employeenumber = $id LIMIT 1";
            $result = mysqli_query($link, $qry) or die(mysqli_error($link));
            $row = mysqli_fetch_assoc($result);//mivel limit 1 volt és egyébként is csak 1 lehet ilyen, ezért nem ciklusban bontjuk ki
        }else{
            //ha nincs id, menjünk a listára vissza (ugyanez a file lista mód)
            header('location:'.$_SERVER['PHP_SELF']);
            exit();
        }
        //kimenet összeállítása
        if(!$row){//ha nincs adatunk, 'hibaüzenet'
            $output .= 'Nincs ilyen alkalmazott';
        }else{//találtunk ilyet, adatok:
            //echo '<pre>'.var_export($row,true).'</pre>';
            $output = '<a href="?">vissza a listázásra</a><ul>';//lista nyitása
            foreach ($row as $fieldName => $data){
                $output .= "<li>$fieldName: <b>$data</b></li>";
            }
            $output .= '</ul>';
        }
        break;
    default:
        $qry = "SELECT employeenumber,firstname,lastname,email,extension,jobtitle FROM employees";
        $result = mysqli_query($link,$qry) or die(mysqli_error($link));

        $output = '<a href="#">új felvitel</a>
<table border="1">
    <tr>
        <th>név</th>
        <th>email</th>
        <th>telefon</th>
        <th>beosztás</th>
        <th>művelet</th>
    </tr>';//itt lesz a tblázat minden eleme (table nyitás+címsor)
//kibontás ciklusban
        while(null !== $row = mysqli_fetch_assoc($result)){
            $output .= '<tr>
                    <td><a href="?action=show&amp;id='.$row['employeenumber'].'">'.$row['firstname'].' '.$row['lastname'].'</a></td>
                    <td>'.$row['email'].'</td>
                    <td>'.$row['extension'].'</td>
                    <td>'.$row['jobtitle'].'</td>
                    <td>módosít | töröl</td>
                </tr>';
        }
//table zárása
        $output .= '</table>';
    break;
}

//output kiírása
echo $output;
//@todo HF: ha el kellen tárolnod egy felhasználót aki regisztrált, milyen minimum adathalmazt regisztrálnál, és azokat milyen adatmezőkbe gyüjtenéd
//@todo másképpen: users adattábla tervezése (szerkezet)
