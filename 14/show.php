<?php
require "database.php";
//url paraméter kinyerése ha van, vagy null/false
$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);


//ha van id-nk akkor kérjük le az adatokat
if ($id) {
    $qry = "SELECT * FROM employees WHERE employeenumber = $id LIMIT 1";
    $result = mysqli_query($link, $qry) or die(mysqli_error($link));
    $row = mysqli_fetch_assoc($result);//mivel limit 1 volt és egyébként is csak 1 lehet ilyen, ezért nem ciklusban bontjuk ki
}else{
    //ha nincs id, menjünk a listára vissza (tablazat.php)
    header('location:tablazat.php');
    exit();
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Alkalmazott adatai</title>
</head>
<body>

Az alkalmazott adatai:
<?php

if(!$row){//ha nincs adatunk, 'hibaüzenet'
    echo 'Nincs ilyen alkalmazott';
}else{//találtunk ilyet, adatok:
    //echo '<pre>'.var_export($row,true).'</pre>';
    $output = '<ul>';//lista nyitása
    foreach ($row as $fieldName => $data){
        $output .= "<li>$fieldName: <b>$data</b></li>";
    }
    $output .= '</ul>';
    //kiírás
    echo $output;
}


?>
</body>
</html>
