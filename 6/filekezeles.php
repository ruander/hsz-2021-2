<?php
$dir = 'test/';//mappa neve, amibe dolgozni szeretnénk

//mappa ellenőrzés/létrehozás
if (!is_dir($dir)) {
    //hozzuk létre a mappát
    mkdir($dir, 0755, true);
}


$fileName = 'helloworld.txt';//ez legyen a file neve

$fileContent = 'Hello World!' . PHP_EOL;//ez legyen a file tartalma

//egyszerű szöveges file kiírása
var_dump(file_put_contents($dir . $fileName, $fileContent/*, FILE_APPEND*/));

//file visszaolvasása
$readContent = file_get_contents($dir . $fileName);
var_dump($readContent);

//nem primitív érték fileba írása nem lehetséges, ezért át kell stringgé konvertálni
$user = [
        'name' => 'Horváth György',
        'email' => 'hgy@iworkshop.hu',
        'status' => 1
    ];
///echo $user;//array
$fileName = 'user.csv';

$handle = fopen($dir . $fileName, "w");//filet 'megnyitjuk'
fputcsv($handle, $user, ';');
fclose($handle);//file lezárása


//csv visszaolvasása
$handle = fopen($dir . $fileName, "r");//filet 'megnyitjuk' olvasásra
//filehossz filesize(path)-al kiolvasható
$readCSV = fgetcsv($handle,/*filesize*/1000,';');//kiolvassuk a csv tartalmat
fclose($handle);//file lezárása
echo '<pre>';
var_dump( $user, $readCSV);
if($readCSV == $user){
    echo 'Tárolás után (csv), ugyanaz';//nem egyeznek!
}
//adattömb átalakítása 1 - serialize()
$userSerialize = serialize($user);
var_dump($userSerialize);
$fileName = 'user.txt';
//tárolás fileban
file_put_contents($dir.$fileName,$userSerialize);

//adatok visszaolvasása
$readSerialized = file_get_contents($dir.$fileName);
$userData = unserialize($readSerialized);//visszaalakítás tömbbé
var_dump($userData);
if($userData === $user){
    echo 'Tárolás után, ugyanaz';//egyeznek!
}

//több dimezió teszt
$user = [
    0 => [
        'name' => 'Horváth György',
        'email' => 'hgy@iworkshop.hu',
        'statusz' => 1
    ],
    1 => [
        'name' => 'test user',
        'email' => 't@t.t',
        'statusz' => 0
    ]
];
$fileContent = serialize($user);
$fileName = 'user.data';
file_put_contents($dir . $fileName, $fileContent);

//visszaolvasás
$readContent = file_get_contents($dir . $fileName);
$userData = unserialize($readContent);
if($userData === $user){
    echo 'Tárolás után, ugyanaz';//egyeznek!
}

/**********************/
/*JSON adatok kezelése*/
/**********************/
$fileName = 'user.json';
//átalakítás json formátumra
$userJson = json_encode($user);
var_dump('<br>',$userJson);
//tárolás fileban
file_put_contents($dir . $fileName, $userJson);

//visszaolvasás
$readJson = file_get_contents($dir . $fileName);
//visszaalakítás
$userData = json_decode($readJson, true);
var_dump($userData);
if($userData === $user){
    echo 'Tárolás után, ugyanaz';//egyeznek!
}
