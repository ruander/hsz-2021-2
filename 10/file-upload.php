<?php
//erőforrások
const VALID_FILE_TYPES = ['image/jpg','image/jpeg', 'application/pdf'];//csak ilyen tipusu filet lehet feltölteni
if (!empty($_POST)) {
    $hiba = [];
    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    //echo '<pre>' . var_export($_FILES, true) . '</pre>';

    //kötelező file mező
    if ($_FILES['fileToUpload']['error'] !== 0) {
        $hiba['fileToUpload'] = '<span class="error">Hiba a feltöltésben!</span>';
    } else {//ha van file feltöltve
        //file mező hibakezelési minták
        $file_limit = 1024 * 1024 * 2;//2MB max fileméret
        if ($_FILES['fileToUpload']['size'] > $file_limit) {
            $hiba['fileToUpload'] = '<span class="error">Hiba a feltöltésben! A file túl nagy!</span>';
        }

        //filetipus: most csak jpeg és pdf, fent van megadva az erőforrásoknál, egy állandó tömb elemeiként
        if(!in_array($_FILES['fileToUpload']['type'],VALID_FILE_TYPES)){
            $hiba['fileToUpload'] = '<span class="error">Hiba a feltöltésben! A file tipusa nem megengedett!</span>';
        }
    }


    if (empty($hiba)) {
        //ha nincs hiba akkor elvileg van, és jó a feltöltött file
        $fileName = $_FILES['fileToUpload']['name'];
        //mappa neve és meglétének ellenőrzése
        $dir = 'uploads/';
        if(!is_dir($dir)){
            mkdir($dir,0755,true);
        }

        //mozgassuk a filet az ideiglenes helyéről (tmp_name) a kívánt helyre és névvel
        if(is_uploaded_file($_FILES['fileToUpload']['tmp_name'])){
            move_uploaded_file($_FILES['fileToUpload']['tmp_name'],$dir.$fileName);
        }


        //die('nincs hiba');

    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,  minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>File feltöltése, Képfeltöltés, képkezelés</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            padding: 15px;
        }

        label {
            display: flex;
            flex-flow: column nowrap;
            margin: 5px 0;
        }

        .error {
            color: red;
            font-style: italic;
            font-size: 0.8em;
        }
    </style>
</head>
<body>
<section>
    <h2>File(ok) feltöltése</h2>
    <form method="post" enctype="multipart/form-data">
        <label>
            <span>File feltöltése (pdf,jpg)</span>
            <input type="file" name="fileToUpload">
            <?php echo getError('fileToUpload');//hibaüzenet kírása saját eljárással ?>
        </label>
        <button name="submit" value="teszt">Mehet</button>
    </form>
</section>
</body>
</html>
<?php
/**
 * Hibaüzenez kiíró eljárás, ha van a $hiba tömbnek az adott kulcsán hibaüzenet akkor visszatér vele
 * @param $fieldName
 * @return false|string
 */
function getError($fieldName)
{
    global $hiba;//az eljárás 'látni fogja'
    if (isset($hiba[$fieldName])) {
        return $hiba[$fieldName];
    }
    return false;//nem volt ilyen elem
}
