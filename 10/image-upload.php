<?php
//erőforrások
const VALID_FILE_TYPES = ['image/jpg','image/jpeg'];//csak ilyen tipusu filet lehet feltölteni
if (!empty($_POST)) {
    $hiba = [];
    //echo '<pre>' . var_export($_POST, true) . '</pre>';
    //echo '<pre>' . var_export($_FILES, true) . '</pre>';
    //@todo HF: HA a kicsinyítendő méretnél kisebb az eredeti kép (most 480) akkor kicsinyítés nélkül menjen fel az eredeti méretében, ha egy képnek bármelyik mérete kisebb mint a thumbnail mérete, akkor hibaüzenet, a kép túl kicsi
    //nem kötelező file mező, de ha van a tipusa legyen jpg I. (átverhető)
    if ($_FILES['fileToUpload']['error'] === 0) {
        //filetipus: most csak jpeg fent van megadva az erőforrásoknál, egy állandó tömb elemeiként
        if (!in_array($_FILES['fileToUpload']['type'], VALID_FILE_TYPES)) {
            $hiba['fileToUpload'] = '<span class="error">Hiba a feltöltésben! A file tipusa nem megengedett!</span>';
        }
        //képtipus ellenőrzése, nem átverhető mime type-al
        $fileInfo = getimagesize($_FILES['fileToUpload']['tmp_name']);
        //echo '<pre>' . var_export($fileInfo, true) . '</pre>';
        if($fileInfo === false){
            $hiba['fileToUpload'] = '<span class="error">Ejnye bejnye!</span>';
        }else{
            //szélesség,magasság hibakezelése
        }
    }




    if (empty($hiba)) {
        //ha nincs hiba akkor elvileg van, és jó a feltöltött file
        $fileName = $_FILES['fileToUpload']['name'];
        //mappa neve és meglétének ellenőrzése
        $dir = 'images/';
        $thumbDir = $dir.'thumbnails/';
        if(!is_dir($thumbDir)){
            mkdir($thumbDir,0755,true);
        }
        //fentebb van deklarálva
        $width=$fileInfo[0];//kép szélessége (hibakezelni fentebb kell!)
        $height=$fileInfo[1];//kép magassága (hibakezelni fentebb kell!)
        $ratio = $width/$height;//képarány
        //var_dump($width,$height,$ratio);
        //die('nincs hiba');
        //kicsinyítés, mindenképp 480px szélességre
        $targetWidth = 480;
        $targetHeight = $targetWidth/$ratio;
        //imagecopyresampled ( GdImage $dst_image , GdImage $src_image , int $dst_x , int $dst_y , int $src_x , int $src_y , int $dst_width , int $dst_height , int $src_width , int $src_height )
        $canvas = imagecreatetruecolor($targetWidth,$targetHeight);//vászon, erre kerül majd a kép
        //forrás (fontos a kép tipusa)
        $src_image = imagecreatefromjpeg($_FILES['fileToUpload']['tmp_name']);
        //koordináták
        $dst_x =0;
        $dst_y = 0;
        $src_x = 0;
        $src_y= 0;
        //képművelet
        imagecopyresampled($canvas,$src_image,$dst_x,$dst_y,$src_x,$src_y,$targetWidth,$targetHeight,$width,$height);
        //átverés!!! a böngésző higgye a filet képnek:
        //header('content-type:image/jpeg');
        imagejpeg($canvas, $dir.$fileName, 100);
        /**************THUMBNAIL******************/
        //@todo HF megoldható e egyszerűbben imagecrop() vagy imagecropauto() használatával?
        //négyzet alakú thumbnail készítése, most 150x150
        $thumbWidth = 150;
        $thumbHeight = 150;
        //új canvas
        $canvas = imagecreatetruecolor($thumbWidth,$thumbHeight);
        if($ratio > 1){
            //a kívánt szélességet ki kell számolni hogy ne torzitsuk a képet
            $targetWidth = $thumbHeight*$ratio;//a maradék 'leesik' a vászonról
            $targetHeight = $thumbHeight;//ez a thumb magassága lesz
            $dst_x = round(($thumbWidth-$targetWidth)/2);//ennyivel kell eltolni hogy a közepéből vágjunk ki
        }else{//álló vagy négyzet kép esetén is számolnunk kell de a másik tengelyen, négyzetnél mindegy
            $targetHeight = $thumbWidth/$ratio;
            $targetWidth = $thumbWidth;
            $dst_y = round(($thumbHeight-$targetHeight)/2);
        }
        imagecopyresampled($canvas,$src_image,$dst_x,$dst_y,$src_x,$src_y,$targetWidth,$targetHeight,$width,$height);
        //header('content-type:image/jpeg');
        imagejpeg($canvas, $thumbDir.$fileName, 80);
        /*********************THUMBNAIL END*************************/
        //takarítás
        imagedestroy($canvas);
        imagedestroy($src_image);
    }
}
?><!doctype html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,  minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>File feltöltése, Képfeltöltés, képkezelés</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        form {
            padding: 15px;
        }

        label {
            display: flex;
            flex-flow: column nowrap;
            margin: 5px 0;
        }

        .error {
            color: red;
            font-style: italic;
            font-size: 0.8em;
        }
    </style>
</head>
<body>
<section>
    <h2>Kép feltöltése</h2>
    <form method="post" enctype="multipart/form-data">
        <label>
            <span>Kép feltöltése (csak jpg)</span>
            <input type="file" name="fileToUpload">
            <?php echo getError('fileToUpload');//hibaüzenet kírása saját eljárással ?>
        </label>
        <button name="submit" value="teszt">Mehet</button>
    </form>
</section>
</body>
</html>
<?php
/**
 * Hibaüzenez kiíró eljárás, ha van a $hiba tömbnek az adott kulcsán hibaüzenet akkor visszatér vele
 * @param $fieldName
 * @return false|string
 */
function getError($fieldName)
{
    global $hiba;//az eljárás 'látni fogja'
    if (isset($hiba[$fieldName])) {
        return $hiba[$fieldName];
    }
    return false;//nem volt ilyen elem
}
